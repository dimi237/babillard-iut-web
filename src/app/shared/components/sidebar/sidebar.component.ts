import { Component, OnInit } from '@angular/core';
import { NbMenuItem, NbSidebarService } from '@nebular/theme';

@Component({
  selector: 'iut-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  state = 'compacted';
  items: NbMenuItem[] = [
    {
      title: 'Accueil',
      icon: 'home-outline',
      url: 'homepage',
    },
    {
      title: 'Templates',
      icon: 'layers-outline',
      url: 'templates'
    },
    {
      title: 'Activités',
      icon: { icon: 'options-outline', pack: 'eva', status: 'primary' },
      url: 'activity'
    },
    {
      title: 'Messages',
      icon: 'email-outline',
      url: 'message'
    },
    {
      title: 'Historiques',
      icon: 'list-outline',
      url: 'historical'
    },
    {
      title: 'Reporting',
      icon: 'trending-up-outline',
      url: 'reporting'
    },
    {
      title: 'Paramètres',
      icon: 'settings-outline',
      url: 'settings'
    },
  ];


  constructor(public sidebarService: NbSidebarService) { }

  ngOnInit(): void {
  }

}
