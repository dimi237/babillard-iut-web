import { AuthService } from './../../services/auth.service';
import { Component, OnInit, TemplateRef, ViewEncapsulation } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { filter } from 'rxjs/internal/operators/filter';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'iut-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {
  currentRout: any;

  id: string;
  pass: string;

  button = {
    'message': 'Se connecter',
    'function': 'open()'
  }
  canShow = false;

  showPassword = true;
  user : any;

  constructor(
    private dialogService: NbDialogService,
    public router: Router,  private toastrSrv: NbToastrService,private aurthSrv :
    AuthService, private storageSrv : StorageService) {
    // tslint:disable-next-line: align
    this.router.events.pipe(filter((event: any) => event instanceof NavigationEnd)).subscribe(() => {
      this.canShow = this.router.url === '/welcome';

    });
  }

  ngOnInit(): void {
    
  }

  getInputType() {
    if (this.showPassword) {
      return 'text';
    }
    return 'password';
  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
  }

  async login(ref: any): Promise<void> {

 await this.aurthSrv.signin({matricule : this.id, password: this.pass});

 ref.close();
    
   
  }


  // tslint:disable-next-line: typede
  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog);
  }

  navigate() {
    this.router.navigate(['/administration']);
  }
  logout() {
    this.router.navigate(['/welcome']);
  }
 
}