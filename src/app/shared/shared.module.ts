import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FormsModule } from '@angular/forms';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NbButtonModule, NbCardModule, NbDialogModule, NbFormFieldModule, NbIconModule, NbInputModule, NbMenuModule, NbSidebarModule} from '@nebular/theme';
import { SidebarComponent } from './components/sidebar/sidebar.component';



@NgModule({
  declarations: [HeaderComponent, SidebarComponent],
  imports: [
    CommonModule,
    FormsModule,
    NbButtonModule,
    NbEvaIconsModule,
    NbIconModule,
    NbDialogModule.forChild(),
    NbCardModule,
    NbInputModule,
    NbSidebarModule,
    NbMenuModule,
    NbFormFieldModule,

  ],
  exports: [HeaderComponent, SidebarComponent]
})
export class SharedModule { }
