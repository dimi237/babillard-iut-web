import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})


export class AuthService {
  BASE_URL: string;

  constructor( private storageSrv: StorageService,
    private router: Router,
    private toastSrv: NbToastrService,
    private http: HttpClient) {
      this.BASE_URL = `http://localhost:3000`;
     }


    
  async signin(credentials: any): Promise<any> {
    try {
      this.storageSrv.clear();
      let authData: any;
      let path : string;

      console.log('tok tok tok  :', credentials);
      
      if (credentials.matricule.length !== 4 && credentials.matricule.length !== 8) {
        this.toastSrv.danger('Vous avez entrer un mauvais matircule ', 'Echec d\'authentification');
        return false;
      }
      if (credentials.matricule.length === 4) {
        authData = await this.http.get(`${this.BASE_URL}/prof`,{}).toPromise();
        path = 'administration';
      }
      if (credentials.matricule.length === 8) {
        authData = await this.http.get(`${this.BASE_URL}/etudiant`,{}).toPromise();  
        path = 'etudiant';      
      }
      console.log(authData);


      const found = authData.find(e=>e.matricule === parseInt(credentials.matricule) || e.matricule === credentials.matricule);
      

      if (!found) {
        this.toastSrv.danger(`Le matricule que vous avez renseignée n'est pas valide.`);
        return false;
      }

      if (found.password !== parseInt(credentials.password)) {
        this.toastSrv.danger(`Le mot de passe que vous avez renseigné n'est pas valide`, 'Mot de passe incorrect');
        return false;        
      }

      console.log('after    ....  :', found);
      this.storageSrv.setObject('user', found);
      this.toastSrv.success('Authentification réussi ', 'Authentification réussi');

      
      this.router.navigate([path], {queryParams: {profId: found._id}});
      // this.updateUserData(authData.user._id);

    } catch (error) {

      if (![400, 403, 404].includes(error.status)) {
        this.toastSrv.danger(`Une erreur s'est produite. Veuillez vérifier votre connexion internet.`);
      }
      console.log(error);
      return Promise.reject(error);
    }
  }

  // async updateUserData(userId: string): Promise<any> {
  //   const url = `${environment.apiUrl}${environment.basePath}/users/${userId}`;
  //   const user: User = await this.http.get<User>(url).toPromise();
  //   return await this.storageSrv.setObject('user', user);
  // }


  async signout(): Promise<any> {
    await this.storageSrv.clear();
    this.router.navigate(['/welcome']);
  }

}
