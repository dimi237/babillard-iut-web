import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class ProfService {
  BASE_URL: string;

  constructor(private http: HttpClient,) { 
    this.BASE_URL = `http://localhost:3000`;
  }

  async saveProf(prof: any): Promise<any> {
    return await this.http.post(`${this.BASE_URL}/prof`, prof).toPromise();
  }

  async getAllProf(): Promise<any> {
    return await this.http.get(`${this.BASE_URL}/prof`).toPromise();
  }

  async getProfById(id: any): Promise<any> {
    let params = new HttpParams();
    if (id) { params = params.append('id', `${id}`); }
    return await this.http.get(`${this.BASE_URL}/prof/${id}`, { params }).toPromise();
  }
}
