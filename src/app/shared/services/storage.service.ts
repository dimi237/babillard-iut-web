import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  setString(key: string, value: string): any {

    localStorage.setItem(key, value);
  }

  getString(key: string): any {

    return localStorage.getItem(key);

  }

  setObject(key: string, value: any): any {
    localStorage.setItem(key, value);
  }

  getObject(key: string): any {

    return localStorage.getItem(key);
  }

  removeItem(key: string): any {
    localStorage.removeItem(key);
  }

  clear(): any { localStorage.clear(); }
}
