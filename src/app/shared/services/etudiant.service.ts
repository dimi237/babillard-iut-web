import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class EtudiantService {

  BASE_URL: string;

  constructor(private http: HttpClient,) { 
    this.BASE_URL = `http://localhost:3000`;
  }

  async saveEtudiant(etudiant: any): Promise<any> {
    return await this.http.post(`${this.BASE_URL}/etudiant`, etudiant).toPromise();
  }

  async getAllEtudiant(): Promise<any> {
    return await this.http.get(`${this.BASE_URL}/etudiant`).toPromise();
  }

  async getEtudiantById(id: any): Promise<any> {
    let params = new HttpParams();
    if (id) { params = params.append('id', `${id}`); }
    return await this.http.get(`${this.BASE_URL}/etudiant/${id}`, { params }).toPromise();
  }

  async getEtudiantBy(param: any): Promise<any> {
    let params = new HttpParams();
    if (param) { params = params.append('class', `${param}`); }
    return await this.http.get(`${this.BASE_URL}/etudiants/`, { params }).toPromise();
  }
}
