import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MatiereService {

  BASE_URL: string;

  constructor(private http: HttpClient,) { 
    this.BASE_URL = `http://localhost:3000`;
  }

  async saveMatiere(matiere: any): Promise<any> {
    return await this.http.post(`${this.BASE_URL}/matiere`, matiere).toPromise();
  }

  async getAllMatiere(): Promise<any> {
    return await this.http.get(`${this.BASE_URL}/matiere`).toPromise();
  }

  async getMatiereById(id: any): Promise<any> {
    let params = new HttpParams();
    if (id) { params = params.append('id', `${id}`); }
    return await this.http.get(`${this.BASE_URL}/matiere/${id}`, { params }).toPromise();


  }
  async getMatiereBy(param: any): Promise<any> {
    let params = new HttpParams();

    let {classname , prof }  = param;
    if (prof) { params = params.append('prof', `${prof}`); }
    if (classname) { params = params.append('class', `${classname}`); }

    return await this.http.get(`${this.BASE_URL}/matieres/`, { params }).toPromise();
  }
}
