import { Component } from '@angular/core';

@Component({
  selector: 'iut-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'babillard-iut-web';
}
