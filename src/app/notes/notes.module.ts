import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotesRoutingModule } from './notes-routing.module';
import { NotesComponent } from './notes.component';
import { SharedModule } from '../shared/shared.module';
import { NbIconModule, NbSelectModule, NbInputModule, NbButtonModule, NbCardModule, NbDatepickerModule, NbListModule } from '@nebular/theme';


@NgModule({
  declarations: [NotesComponent],
  imports: [
    CommonModule,
    NotesRoutingModule,
    CommonModule,
    SharedModule,
    NbIconModule,
    NbSelectModule,
    NbInputModule,
    NbButtonModule,
    NbCardModule,
    NbDatepickerModule,
    NbListModule,
  ]
})
export class NotesModule { }
