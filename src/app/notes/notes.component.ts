import { EtudiantService } from './../shared/services/etudiant.service';
import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'iut-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {


  selectedItem = '';
  dateTimePicker: any;
  etudiants: any[];
  profId: any;
  classname: any;
  constructor(private route: ActivatedRoute, private location: Location, private etudiantSrv: EtudiantService) { }


  async ngOnInit(): Promise<void> {

    this.profId = this.route.snapshot.queryParams['profId'];
    this.classname = this.route.snapshot.queryParams['classname'];

    this.etudiants =  await this.etudiantSrv.getEtudiantBy(this.classname);
    console.log(this.etudiants);
    

  }
  back(){
    this.location.back();
  }

}
