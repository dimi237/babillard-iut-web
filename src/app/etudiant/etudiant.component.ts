import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';
import { EtudiantService } from '../shared/services/etudiant.service';
import { MatiereService } from '../shared/services/matiere.service';

@Component({
  selector: 'iut-etudiant',
  templateUrl: './etudiant.component.html',
  styleUrls: ['./etudiant.component.scss']
})
export class EtudiantComponent implements OnInit {

 
  selectedItem = '';
  dateTimePicker: any;
  matieres: any[];
  idEtudiant: any;
  etudiant: any;
  classname: any;
  constructor(private route: ActivatedRoute, private location: Location, private etudiantSrv: EtudiantService, private matiereSrv: MatiereService) { }


  async ngOnInit(): Promise<void> {

    this.idEtudiant = this.route.snapshot.queryParams['profId'];

    this.etudiant =  await this.etudiantSrv.getEtudiantById(this.idEtudiant);
    
    this.matieres = await this.matiereSrv.getMatiereBy({classname : this.etudiant.class});
    console.log(this.matieres);

  }
  back(){
    this.location.back();
  }

}
