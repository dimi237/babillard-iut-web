import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EtudiantRoutingModule } from './etudiant-routing.module';
import { EtudiantComponent } from './etudiant.component';
import { NotesRoutingModule } from '../notes/notes-routing.module';
import { NbIconModule, NbSelectModule, NbInputModule, NbButtonModule, NbCardModule, NbDatepickerModule, NbListModule } from '@nebular/theme';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [EtudiantComponent],
  imports: [
    CommonModule,
    EtudiantRoutingModule,
    CommonModule,
    NotesRoutingModule,
    SharedModule,
    NbIconModule,
    NbSelectModule,
    NbInputModule,
    NbButtonModule,
    NbCardModule,
    NbDatepickerModule,
    NbListModule,
  ]
})
export class EtudiantModule { }
