import { MatiereService } from './../shared/services/matiere.service';
import { ProfService } from './../shared/services/prof.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'iut-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.scss']
})
export class AdministrationComponent implements OnInit {

  courses: any[];
  profId: any;
  prof: any;

  constructor(private route: ActivatedRoute,private router:Router, private profSrv: ProfService, private matiereSrv : MatiereService) { }

  async ngOnInit(): Promise<void> {

 
    this.profId = this.route.snapshot.queryParams['profId'];


    this.prof = await this.profSrv.getProfById(this.profId);

    this.courses= await this.matiereSrv.getMatiereBy({prof : this.profId});

    console.log(this.courses);
    

   



  }

  navigate(classname){
    this.router.navigate(['notes'], {queryParams: {profId:this.profId, classname:classname }});
  }



}
